$(function(){

    var mainVideo = $('video#mainvideo');

    /**
    * Scaling and positioning main frame
    */
    var $el = $("#slides");
    var elHeight = $el.outerHeight();
    var elWidth = $el.outerWidth();

    var $nav = $("#navigation");
    var navWidth = $nav.outerWidth();

    var $wrapper = $("#wrapper");

    var navBarOn = false;

    function doResize() {

        var navScale = $wrapper.outerWidth() / 1200;
        navScale = (navScale > 1) ? 1 : navScale;

        $nav.css({
            transform: "translateX(-50%) " + "scale(" + navScale + ")"
        });

        var navHeight = $nav.outerHeight() * navScale;

        var scale, targetHeight, videoOffsetTop;

        targetHeight = (navBarOn) ? ($wrapper.outerHeight() - navHeight) : $wrapper.outerHeight();
        videoOffsetTop = (navBarOn) ? navHeight + 'px' : 0;

        scale = Math.max(
            $wrapper.outerWidth() / elWidth,
            targetHeight / elHeight
        );
        scale = (scale > 1) ? 1 : scale;

        $el.css({
            transform: "translate(-50%, " +  videoOffsetTop + ") " + "scale(" + scale + ")"
        });
    }
    doResize();
    $(window).resize(function(){
        doResize();
    });

    /*setTimeout(function(){
        navBarOn = true;
        doResize();
        $nav.addClass('appear');
    },16400);*/

    /**
     * показывать / скрывать навигацию
     */
    mainVideo.bind('timeupdate', function(){
        var startTime = 29200;
        var moment = this.currentTime * 1000;

        if ((moment >= startTime) && !$('.navigation').hasClass('appear')) {
            navBarOn = true;
            doResize();
            $nav.addClass('appear');
        }
        if ((moment < startTime) && $('.navigation').hasClass('appear')) {
            navBarOn = false;
            doResize();
            $nav.removeClass('appear');
        }
    });

    /*
    * Video functionality
    * */

    /**
     * Переход на конкретный мемент времени видео
     * @param moment
     */
    function goTo(moment) {
        mainVideo[0].currentTime = parseInt(moment);
    }
    goTo(5.9);
    //goTo(288);
    //mainVideo[0].pause();

    mainVideo[0].oncanplaythrough = function() {
        $('.preloader').hide();
        console.log('canplaythrough');
    };
    mainVideo[0].onseeking = function(){
        $('.preloader').show();
        console.log('seeking');
    };

    /**
     * натсройки навигации
     */
    $('#logo').click(function(){
        goTo(30);
        return false;
    });

    $('.navigation a').click(function(){
        clearElements();
        mainVideo[0].play();
        return false;
    });

    $('#nav-beginning').click(function(){
        goTo(30);
        return false;
    });
    $('#nav-promo').click(function(){
        goTo(36.9);
        return false;
    });
    $('#nav-content').click(function(){
        goTo(156);
        return false;
    });
    $('#nav-request').click(function(){
        goTo(386);
        return false;
    });

    $('#nav-hp').click(function(){
        goTo(40);
        return false;
    });
    $('#nav-dacha').click(function(){
        goTo(71);
        return false;
    });
    $('#nav-unicredit').click(function(){
        goTo(114);
        return false;
    });
    $('#nav-aimol').click(function(){
        goTo(168.9);
        return false;
    });
    $('#nav-vetac').click(function(){
        goTo(228);
        return false;
    });
    $('#nav-siberian').click(function(){
        goTo(288);
        return false;
    });

    /**
     * Инициализация кнопки
     * @param id ID html элемента
     * @param timeGoTo время на которое надо перейти по клику
     * @param className класс появления кнопки
     * @param startTime время в которое должна появиться кнопка относительно тайминга видео
     * @param duration вместе с startTime рассчитывает время по таймингу видео когда кнопку надо скрыть
     * @param classOutName класс исчезновения кнопки
     * @param stopVideoOnAppear поставить видео на паузу после появления кнопки
     */
    function initBtn(id,timeGoTo = 0,className = 'fadeIn',startTime = 0,duration = 0,classOutName = 'fadeOut', stopVideoOnAppear = false) {
        var endTime = startTime + duration;
        var cancelPause = false;

        mainVideo.bind('timeupdate', function(){
            var moment = this.currentTime * 1000;
            //console.log(moment);
            if ((moment >= startTime) && (moment <= endTime) && !$(id).hasClass('animated')) {
                cancelPause = false;
                $(id).css({'visibility':'visible'}).addClass(className + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                    if (stopVideoOnAppear && !cancelPause) {
                        mainVideo[0].pause();
                    }
                });
            }
            if (((moment > endTime) || (moment < startTime)) && $(id).hasClass('animated')) {
                $(id).removeClass(className).addClass(classOutName).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                    $(this).css({'visibility':'hidden'}).removeClass('animated');
                });
            }
        });

        $(id).click(function(){

            if (Math.abs(timeGoTo - endTime) > 1000) {
                clearElements();
            }
            goTo(parseInt(timeGoTo) / 1000);
            mainVideo[0].play();
            cancelPause = true;

            return false;

        });
    }

    function initTextElement(id,classInName = 'fadeIn',startTime = 0,duration = 0,blurEffect = false,classOutName = 'fadeOut') {

    }

    /**
     * Инициализация выносок внутри проекта
     * @param id ID html элемента
     * @param startTime время в которое должен появиться тултип относительно тайминга видео
     * @param endTime время в которое должен исчезнуть тултип относительно тайминга видео
     */
    function initTooltip(id,startTime = 0,endTime = 0) {
        mainVideo.bind('timeupdate', function(){
            var moment = this.currentTime * 1000;
            //console.log(moment);
            if ((moment >= startTime) && (moment <= endTime) && !$(id).hasClass('animated')) {
                $(id).css({'visibility':'visible'}).addClass('in animated');
            }
            if (((moment > endTime) || (moment < startTime)) && $(id).hasClass('animated')) {
                $(id).removeClass('in').addClass('out').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                    $(this).css({'visibility':'hidden'}).removeClass('animated');
                });
            }
        });
    }

    /**
     * Инциализация карточек прина первом экране прокта
     * @param id ID html элемента
     * @param startTime время в которое должна появиться карточка относительно тайминга видео
     * @param endTime время в которое должна исчезнуть карточка относительно тайминга видео
     */
    function initCard(id,startTime = 0,endTime = 0) {
        mainVideo.bind('timeupdate', function(){
            var moment = this.currentTime * 1000;
            //console.log(moment);
            if ((moment >= startTime) && (moment <= endTime) && !$(id).hasClass('animated')) {
                $(id).css({'visibility':'visible'}).addClass('in animated');
            }
            if (((moment > endTime) || (moment < startTime)) && $(id).hasClass('animated')) {
                $(id).removeClass('in').addClass('out').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                    $(this).css({'visibility':'hidden'}).removeClass('animated');
                });
            }
        });
    }

    /**
     * Кнопка пропустить
     */
    initBtn('#skip-intro',30000,'fadeIn',5900,24100,'fadeOut');

    /**
     * Кнопки выбора разделов контен / промо
     */
    initBtn('#projects-content',156000,'flipInX',30500,3500,'flipOutX');
    initBtn('#projects-promo',34900,'flipInX',30850,3150,'flipOutX', true);

    /**
     * Кнопка просмотреть контент проекты
     */
    initBtn('#projects-content-more',39000,'flipInY',37100,1600,'flipOutX', true);

    /**
     * просмотреть проект
     */
    initBtn('#hp-more',46900,'zoomInDown',42300,3700,'fadeOut');
    //initBtn('#hp-next',71000,'zoomInDown',42700,3300,'fadeOut', true);
    /**
     * Кнопка следующий проект
     */
    initBtn('#hp-next-proj',71000,'flipInX',43400,26000,'flipOutX',true);

    /**
     * Кнопки далее на внутренних экранах проекта
     */
    initBtn('#hp-pause-1',55700,'flipInX',54200,1500,'fadeOut',true);
    initBtn('#hp-pause-2',64500,'flipInX',63000,1500,'fadeOut',true);
    initBtn('#hp-pause-3',69500,'flipInX',68000,1500,'fadeOut',true);

    //initBtn('#hp-back-2',50000,'flipInX',63000,1500,'fadeOut');
    //initBtn('#hp-back-3',55700,'flipInX',68000,1500,'fadeOut');

    /**
     * просмотреть проект
     */
    initBtn('#dacha-more',77900,'zoomInDown',74300,2700,'fadeOut');
    //initBtn('#dacha-next',114000,'zoomInDown',74600,2400,'fadeOut', true);
    /**
     * Кнопка следующий / предыдущий проект
     */
    initBtn('#dacha-prev-proj',40000,'flipInX',75350,37950,'flipOutX');
    initBtn('#dacha-next-proj',114000,'flipInX',75500,37800,'flipOutX', true);

    /**
     * Кнопки далее на внутренних экранах проекта
     */
    initBtn('#dacha-pause-1',85000,'flipInX',83000,1500,'fadeOut',true);
    initBtn('#dacha-pause-2',92500,'flipInX',91000,1500,'fadeOut',true);
    initBtn('#dacha-pause-3',101500,'flipInX',100000,1500,'fadeOut',true);
    initBtn('#dacha-pause-4',106500,'flipInX',105000,1500,'fadeOut',true);
    initBtn('#dacha-pause-5',113500,'flipInX',112000,1500,'fadeOut',true);

    /**
     * просмотреть проект
     */
    initBtn('#unicredit-more',123000,'zoomInDown',118200,3800,'fadeOut');
    //initBtn('#unicredit-next',162000,'zoomInDown',118400,3600,'fadeOut', true);
    initBtn('#unicredit-prev-proj',71900,'flipInX',119100,35050,'flipOutX');
    /**
     * Кнопка перехода в контен проекты
     */
    initBtn('#unicredit-next-section',162000,'flipInX',119350,34800,'flipOutX', true);

    /**
     * предформа
     */
    initBtn('#unicredit-request',374000,'flipInX',119350,34800,'flipOutX', true);

    /**
     * Кнопки далее на внутренних экранах проекта
     */
    initBtn('#unicredit-pause-1',131500,'flipInX',130000,1500,'fadeOut',true);
    initBtn('#unicredit-pause-2',138500,'flipInX',137000,1500,'fadeOut',true);
    initBtn('#unicredit-pause-3',148500,'flipInX',147000,1500,'fadeOut',true);
    initBtn('#unicredit-pause-4',154500,'flipInX',152800,1500,'fadeOut',true);

    /**
     * Кнопка просмотреть контент проекты
     */
    initBtn('#projects-promo-more',166000,'flipInY',157000,8700,'flipOutX', true);

    /**
     * просмотреть проект
     */
    initBtn('#aimol-more',172900,'zoomInDown',169500,2600,'fadeOut');
    //initBtn('#aimol-next',228000,'zoomInDown',169700,2400,'fadeOut', true);
    /**
     * Кнопка следующий проект
     */
    initBtn('#aimol-next-proj',228000,'flipInX',170300,55000,'flipOutX', true);

    /**
     * Кнопки далее на внутренних экранах проекта
     */
    initBtn('#aimol-pause-1',180500,'flipInX',179000,1500,'fadeOut',true);
    initBtn('#aimol-pause-2',188500,'flipInX',187000,1500,'fadeOut',true);
    initBtn('#aimol-pause-3',195000,'flipInX',193500,1500,'fadeOut',true);
    initBtn('#aimol-pause-4',200500,'flipInX',199000,1500,'fadeOut',true);
    initBtn('#aimol-pause-5',206500,'flipInX',205000,1500,'fadeOut',true);
    initBtn('#aimol-pause-6',213000,'flipInX',211500,1500,'fadeOut',true);
    initBtn('#aimol-pause-7',219000,'flipInX',217500,1500,'fadeOut',true);
    initBtn('#aimol-pause-8',225500,'flipInX',224000,1500,'fadeOut',true);

    /**
     * просмотреть проект
     */
    initBtn('#vetac-more',237990,'zoomInDown',228700,8300,'fadeOut');
    //initBtn('#vetac-next',288000,'zoomInDown',228900,8100,'fadeOut', true);
    /**
     * Кнопка следующий проект
     */
    initBtn('#va-prev-proj',168900,'flipInX',229600,57900,'flipOutX');
    initBtn('#va-next-proj',288000,'flipInX',229800,57700,'flipOutX', true);

    /**
     * Кнопки далее на внутренних экранах проекта
     */
    initBtn('#va-pause-1',246500,'flipInX',245000,1500,'fadeOut',true);
    initBtn('#va-pause-2',255500,'flipInX',254000,1500,'fadeOut',true);
    initBtn('#va-pause-3',264500,'flipInX',263000,1500,'fadeOut',true);
    initBtn('#va-pause-4',274500,'flipInX',273000,1500,'fadeOut',true);
    initBtn('#va-pause-5',284500,'flipInX',283000,1500,'fadeOut',true);

    /**
     * просмотреть проект
     */
    initBtn('#siberian-more',295700,'zoomInDown',289000,6000,'fadeOut');
    //initBtn('#siberian-next',374000,'zoomInDown',289200,5800,'fadeOut', true);
    /**
     * Кнопка следующий проект
     */
    initBtn('#sib-prev-proj',228000,'flipInX',289550,82000,'flipOutX');
    /**
     * Кнопка перехода в промо проекты
     */
    initBtn('#sib-next-section',36900,'flipInX',289750,82000,'flipOutX', true);
    /**
     * предформа
     */
    initBtn('#sib-request',374000,'flipInX',289750,82000,'flipOutX', true);

    /**
     * Кнопки далее на внутренних экранах проекта
     */
    initBtn('#siberian-pause-1',302500,'flipInX',301000,1500,'fadeOut',true);
    initBtn('#siberian-pause-2',309500,'flipInX',308000,1500,'fadeOut',true);
    initBtn('#siberian-pause-3',316500,'flipInX',315000,1500,'fadeOut',true);
    initBtn('#siberian-pause-4',322500,'flipInX',321000,1500,'fadeOut',true);
    initBtn('#siberian-pause-5',328500,'flipInX',327000,1500,'fadeOut',true);
    initBtn('#siberian-pause-6',335500,'flipInX',334000,1500,'fadeOut',true);
    initBtn('#siberian-pause-7',342500,'flipInX',341000,1500,'fadeOut',true);
    initBtn('#siberian-pause-8',353500,'flipInX',352000,1500,'fadeOut',true);
    initBtn('#siberian-pause-9',362500,'flipInX',361000,1500,'fadeOut',true);
    initBtn('#siberian-pause-10',371500,'flipInX',370000,1500,'fadeOut',true);

    /**
     * перейти на экран оставления заявки - контакты и кнопка для появления формы
     */
    initBtn('#begin',384300,'flipInX',375950,8600,'flipOutX', true);

    /**
     * открыть форму - попап
     */
    initBtn('#request',394000,'flipInX',387300,6600,'flipOutX', true);

    function appearElement(id) {

    }

    initTooltip('#hp-tooltip-1',50500,55700);
    initTooltip('#hp-tooltip-2',52250,55700);
    initTooltip('#hp-tooltip-3',52900,55700);
    initTooltip('#hp-tooltip-4',53900,55700);

    initTooltip('#hp-tooltip-5',59150,64740);
    initTooltip('#hp-tooltip-6',60100,64740);
    initTooltip('#hp-tooltip-7',60800,64740);
    initTooltip('#hp-tooltip-8',61600,64740);
    initTooltip('#hp-tooltip-9',62100,64740);
    initTooltip('#hp-tooltip-10',63000,64740);

    initTooltip('#hp-tooltip-11',67600,70500);

    initTooltip('#dacha-tooltip-1',82000,85600);
    initTooltip('#dacha-tooltip-2',82500,85600);

    initTooltip('#dacha-tooltip-3',90000,93920);
    initTooltip('#dacha-tooltip-4',90400,93920);

    initTooltip('#dacha-tooltip-5',97950,101500);
    initTooltip('#dacha-tooltip-6',98010,101500);

    initTooltip('#dacha-tooltip-7',104800,107250);

    initTooltip('#dacha-tooltip-8',110700,113300);

    initTooltip('#unicredit-tooltip-1',126500,131600);
    initTooltip('#unicredit-tooltip-2',127750,131600);
    initTooltip('#unicredit-tooltip-3',127850,131600);

    initTooltip('#unicredit-tooltip-4',134800,138800);

    initTooltip('#unicredit-tooltip-5',142530,148200);
    initTooltip('#unicredit-tooltip-6',143400,148200);
    initTooltip('#unicredit-tooltip-7',144210,148200);
    initTooltip('#unicredit-tooltip-8',144520,148200);
    initTooltip('#unicredit-tooltip-9',145000,148200);
    initTooltip('#unicredit-tooltip-10',146000,148200);

    initTooltip('#unicredit-tooltip-11',152450,155200);
    initTooltip('#unicredit-tooltip-12',153000,155200);

    initTooltip('#aimol-tooltip-1',176680,181480);
    initTooltip('#aimol-tooltip-2',177360,181480);
    initTooltip('#aimol-tooltip-3',178000,181480);

    initTooltip('#aimol-tooltip-4',185230,188500);
    initTooltip('#aimol-tooltip-5',186000,188500);

    initTooltip('#aimol-tooltip-7',198700,201000);

    initTooltip('#aimol-tooltip-8',204400,207000);

    initTooltip('#aimol-tooltip-9',210800,213250);

    initTooltip('#aimol-tooltip-10',216300,219350);
    initTooltip('#aimol-tooltip-11',216800,219350);

    initTooltip('#aimol-tooltip-12',222800,226650);
    initTooltip('#aimol-tooltip-13',223500,226650);
    initTooltip('#aimol-tooltip-14',223900,226650);

    initTooltip('#vetac-tooltip-1',243440,247600);

    initTooltip('#vetac-tooltip-2',252600,257300);

    initTooltip('#vetac-tooltip-3',262300,266700);

    initTooltip('#vetac-tooltip-4',271900,276400);

    initTooltip('#vetac-tooltip-5',281750,287200);

    initTooltip('#vetac-tooltip-5',281750,287200);

    initTooltip('#siberian-tooltip-1',300100,303100);

    initTooltip('#siberian-tooltip-2',307000,310000);

    initTooltip('#siberian-tooltip-3',313300,316400);

    initTooltip('#siberian-tooltip-4',320000,323200);

    initTooltip('#siberian-tooltip-5',326800,329600);

    initTooltip('#siberian-tooltip-6',333100,336000);

    initTooltip('#siberian-tooltip-7',340200,344500);

    initTooltip('#siberian-tooltip-8',350000,355600);
    initTooltip('#siberian-tooltip-9',350700,355600);

    initTooltip('#siberian-tooltip-10',360700,364900);

    initTooltip('#siberian-tooltip-11',369900,373000);


    initCard('#hp-card-1',41500, 46000);
    initCard('#hp-card-2',42000, 46000);
    initCard('#hp-card-3',42450, 46000);

    initCard('#dacha-card-1',73200, 77200);
    initCard('#dacha-card-2',73200, 77200);
    initCard('#dacha-card-3',73600, 77200);
    initCard('#dacha-card-4',74000, 77200);
    initCard('#dacha-card-5',74200, 77200);

    initCard('#unicredit-card-1',117400, 122000);
    initCard('#unicredit-card-2',117500, 122000);
    initCard('#unicredit-card-3',117600, 122000);
    initCard('#unicredit-card-4',117900, 122000);

    initCard('#aimol-card-1',168900, 172200);
    initCard('#aimol-card-2',169100, 172200);
    initCard('#aimol-card-3',169200, 172200);
    initCard('#aimol-card-4',169250, 172200);

    initCard('#vetac-card-1',227900, 236600);
    initCard('#vetac-card-2',228050, 236600);
    initCard('#vetac-card-3',228350, 236600);
    initCard('#vetac-card-4',228350, 236600);

    initCard('#siberian-card-1',289000, 294000);
    initCard('#siberian-card-2',289050, 294000);
    initCard('#siberian-card-4',289250, 294000);

    /**
     * появления / исчесновения адреса на странице заявки
     */
    mainVideo.bind('timeupdate', function(){
        var startTime = 386420;
        var moment = this.currentTime * 1000;

        if ((moment >= startTime) && (moment <= (startTime + 3500)) && !$('.address-block').hasClass('animated')) {
            $('.address-block').css({'visibility':'visible'}).addClass('fadeIn animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){

            });
        }
        if (((moment < startTime) || (moment > (startTime + 3500))) && $('.address-block').hasClass('animated')) {
            $('.address-block').removeClass('fadeIn').addClass('fadeOut').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $(this).css({'visibility':'hidden'}).removeClass('animated');
            });
        }
    });

    /**
     * появления / исчесновение формы на странице заявки
     */
    mainVideo.bind('timeupdate', function(){
        var startTime = 394080;
        var moment = this.currentTime * 1000;

        if ((moment >= startTime) && (moment <= (startTime + 1600)) && !$('.request-form').hasClass('animated')) {
            $('.request-form').css({'visibility':'visible'}).addClass('bounceIn animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                mainVideo[0].pause();
            });
        }
        if (((moment < startTime) || (moment > (startTime + 1600))) && $('.request-form').hasClass('animated')) {
            $('.request-form').removeClass('fadeIn').addClass('bounceOut').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $(this).css({'visibility':'hidden'}).removeClass('animated');
            });
        }
    });

    $('#request-form-submit').click(function(){
        formSubmit();
    });

    function formSubmit(){
        mainVideo[0].play();
    }

    /**
     * убрать все висячие элемены - карточки, тултипы и кнопки
     */
    function clearElements() {
        $('.controls .btn, .playback-controls .btn, .address-block, .tooltip, .card').css({'visibility':'hidden'}).removeClass('animated fadeIn fadeOut flipInX flipOutX flipInY flipOutY zoomInDown zoomOutDown in out');
    }

});